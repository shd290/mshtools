# Medion Smart  Home Sensoren Anbindung an Raspberry Pi  über BLE

Das folgende Tool Set ist eine technische Demonstration und dient ausschliesslich zum Testen. 
Verwendung und Benutzung auf eigene Gefahr und Risiko.  

Die Kommunikation der Sensoren wie Bewegungsmelder, Tuersensoren und Rauchmelder erfolgt verschluesselt ueber BLE an die Zentrale. Das Tool  Set ermoeglicht das lesen der folgenden Sensoren:

- Bewegungsmelder
- Tuersensoren
- Rauchmelder

Zusaetzlich kann das Tool Set ein Alarmsignal an den Rauchmelder senden.  

Die Kommunikation erfolgt dabei ueber BLE. Eine Internetverbindung ist nur zum initialen Aufsetzen notwendig.

In meiner Konfiguration verwende ich eine Raspberry Pi 3 B Rev als zentralen Datensammler. Zusaetzlich werden pro Etage noch weitere Raspberry Pi Zero W verwendet.  Damit wird es ermoeglich kostenguenstig einen weiten Bereich abzudecken. Alle Daten werden an einer zentralen Stelle gesammelt und koennen weiterverarbeitet werden. (MQTT Server)

Das System der Tools besteht aus 2 Komponenten:

## 1.  SH_GetCloudData

Dieses Programm verbindet sich mit Hilfe eure Email/Passwort mit dem Smart Home Cloud Server und liest dabei eine Liste von Sensoren, MACs und BLE Keys. Diese werden in einer Datei
Smarthome.json gespeichert. Wenn neue Sensoren hinzugefuegt werden, muss dieses Programm nochmal die Sensordaten aus der Cloud auslesen. Der Aufruf dieses Programmes erfolgt normalerweise sehr selten.

## 2. SH_CheckBLEData

Dieses Programm wir auf jeder Raspberry Pi ausgefuehrt. Diese SW entschluesselt dabei mit Hilfe der BLE Keys die BLE Kommunikation zur Zentrale. Die Daten werden an einen MQTT Server gesendet. Auch ist es moeglich einen Alarm der RM auszuloesen.

# Beispiel und Installation

Eine Raspberry Pi 3 besitzt die IP 192.168.3.27  und ueberwacht das Erdgeschoss.  Auf dieser Raspi Pi laeuft OpenHab und ist ein MQTT server (Mosquitto) installiert ohne Authentifikation. 
Eine Raspberry  Pi Zero W besitzt die IP 192.168.3.28 und ueberwacht das Obergeschoss.
Die Boxen sind so eingestellt, dass ausschliesslich das CLI Interface startet. (Performance Gruenden)
Zum Ausfuehren der Befehle wird ssH ueber Putty verwendet, zur Dateiuebertragung wird WinSCP benutzt.

## Vorbereitung auf den Raspberry Pi s


Folgende Python3 Pakete installieren:

```sh
sudo pip3 install bluepy
sudo pip3 install pyarmor
sudo pip3 install paramiko
sudo pip3 install paho-mqtt
```

## Schritte Raspberry Pi 3 und Raspberry Zero W

in dieses Verzeichnis cd ~ die folgende Datei kopieren (z.B. mit WinSCP). 
SH_Tools.zip
```sh
unzip SH_Tools.zip
cd SH
python3 SH_GetCloudData.py
```

Jetzt registrierte Email und Passwort eingeben. Nach Programmende sollte eine Datei exisiteren smarthome.json.

Testweise folgendes Programm ausfuehren:
```sh
sudo python3 SH_CheckBLE .py -n Test -d
```
Ausgabemeldungen durchlesen und Hinweise zustimmen. <Strg>c Programm unterbrechen.


## MQTT Server verbinden Raspberry 3

```sh
cd SH
sudo nohup python3 SH_CheckBLE.py -n EG -mqtt 192.168.3.27 &
```
optional mit User/Passwort  -mqtt user@192.168.3.27 -password DeinPasswort

## MQTT Server verbinden Raspberry Zero W

```sh
cd SH
sudo nohup python3 SH_CheckBLE.py -n OG -mqtt 192.168.3.27 &
```
optional mit User/Passwort  -mqtt user@192.168.3.27 -password DeinPasswort

## MQTT Hinweise:

Mosquitto Setup wurde in openhab mit sudo openhabian-config installiert. Warnungen zur Verwendung des hauseigenes MQTT Servers wurden ignoriert.
Zur Analyse der MQTT Verbindungen wuerde ich das Tool MQTT.fx empfehlen.

## MQTT Sensor Ausgaben

Fuer jeden Sensor wird in der Cloud einen Namen vergeben. Dieser Name sollte fuer diese Tools moeglichst ohne Umlaute/Sonderzeichen/Leerzeichen sein.
Die uebertragung erfolgt ueber Subscribe mit dem folgenden Topic:
msh/d_{NameSensor)  Beispiel mit dem Name motion1
msh/d_motion1  (Kleinbuchstaben)
Die Daten werden ueber das JSON Format uebertragen.

## MQTT Rauchmelder Alarm ausloesen

Die Ansteuerung erfolgt ueber Publish message mit dem folgenden Topic:
             msh/{raspiname}/ctrl
             Beispiel im OG soll die RaspiPi OG den Rauchmelder ausloesen:
              msh/OG/ctrl mit dem folgendem JSON Inhalt: {"cmd":"SD","mac":"a0a1a2a3a4a5"}. 
      “OG und a0a1a2a3a4a5” sind hier nur ein Beispiel. Die mac Adressen koennen aus dem File smarthome.json entnommen werden.

# Verwendete HW/SW

**Raspberry Pi 3 Model B Rev 1.2 a02082 (Sony, UK)**
debian_version  10.2
[https://github.com/openhab/openhabian/releases/tag/v1.5](https://github.com/openhab/openhabian/releases/tag/v1.5) 
openhabian-pi-raspbian-201908050414-gitca0976f-crc6a66b5a1.img
PiSetup: start only CLI Interface, ssH enabled



**Raspberry Pi Zero W Rev 1.1  9000c1**
debian_version  10.1
2019-09-26-raspbian-buster.img   3.8 GB Image size  Desktop Version 
PiSetup: start only CLI Interface, ssH enabled



## Spenden

Es waere schoen wenn ihr mich 
unter folgender Bitcoin Adresse: **1BSv43wsjcX7C6xfzxnDSV8coCxRAVKrgf** unterstuezt.

