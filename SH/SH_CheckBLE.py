import time
import json
import argparse
import os
import subprocess
import paho.mqtt.publish as publish
import paho.mqtt.subscribe as subscribe
import threading
import re

from bluepy import btle

from SH_Lib import DecryptBLE, EncryptBLE, EncryptSD

import warnings

warnings.filterwarnings("ignore")

dictKeys = {}
dictNames = {}
dictType = {}
dictRXCount = {}
dictRXLevel = {}
dictLast = {}
listSD = []

DictBuffer = []

startfound = 0
endfound = 0
datafound = 0
debuginfo = False
usemqtt = False
cputs = 0
process_hcitool = None
process_btmon = None
process_started = False
alarmList = []
raspiname = "unusedName"
stopProcess = False
sdMac = ""
sdStart = False
hostadr = ""

def checkSubscribeMSGs():
    global stopProcess, raspiname, listSD, sdMac, sdStart, hostadr, debuginfo
    while (not stopProcess):
        try:
            msg = subscribe.simple("msh/{0}/ctrl".format(raspiname), qos=0, msg_count=1, retained=False, hostname="{0}".format(hostadr))
            msg = msg.payload.decode()
            if debuginfo:
                print(msg)
            msg_dict = json.loads(msg)
            if msg_dict['cmd'] == 'SD':
                if msg_dict['mac'].upper() in listSD:
                    sdMac = msg_dict['mac'].upper()
                    sdStart = True
            if msg_dict['cmd'] == 'stopProcess':
                stopProcess = True
                
        except:
            pass
    print('Subscribe thread finished')

def processSDrequests():
    global sdMac, sdStart, process_started, process_btmon, process_hcitool
    if sdStart:
        sdStart = False
        if process_started:
            process_started = False
            process_btmon.kill()
            process_hcitool.kill()
            time.sleep(0.2)
        alarmSD(sdMac)


def alarmSD(mac):
    global dictKeys, debuginfo
    try:
        key = dictKeys[mac]
        if debuginfo:
            print('Alarm')
        device = btle.Peripheral(None, addrType=btle.ADDR_TYPE_PUBLIC)
        physmac = mac[0:2]+':'+mac[2:4]+':'+mac[4:6]+':'+mac[6:8]+':'+mac[8:10]+':'+mac[10:12]
        device.connect(physmac)
        readValue = device.readCharacteristic(0x12)
        readValue = readValue.hex()[8:]
        message = DecryptBLE(readValue.upper(), key, mac)
        message = EncryptSD(message, key, mac)
        device.writeCharacteristic(0x12, bytearray.fromhex(message), withResponse=True)
        readValue = device.readCharacteristic(0x12)
        readValue = readValue.hex()[8:]
        device.writeCharacteristic(0x21, bytearray.fromhex('0300'), withResponse=True)
        device.disconnect()
        time.sleep(0.2)
    except:
        pass


def cputemp():
    temp = 0
    try:
        f = open('/sys/class/thermal/thermal_zone0/temp','r')
        for line in f:
            temp = int(line)
            break
        f.close()
    except:
        temp = 0
    return temp*1.0/1000.0


parser = argparse.ArgumentParser()
parser.add_argument("-mqtt", help="mqtt hostname e.g. user@192.168.3.27 follows by -pw or only 192.168.3.27 without pw", type=str)
parser.add_argument("-password", help="mqtt password", type=str)
parser.add_argument('-d', help="print some debug infos", action='store_true')
parser.add_argument('-n', help="set the name of raspberry pi in sensor information", type=str)

args = parser.parse_args()
if args.mqtt and not args.password:
    if '@' in args.mqtt:
        print(args.mqtt)
        print('password is missing')
        exit(0)
if args.n:
    raspiname = str(args.n)
if args.mqtt:
    usemqtt = True
if args.d:
    debuginfo = True
auth = None
if usemqtt:
    hostadr = args.mqtt
    if '@' in args.mqtt:
        auth = dict(username=args.mqtt('@')[0], password= args.password)
        hostadr = args.mqtt('@')[1]

if debuginfo:
    print('CPU Temp: {0}'.format(cputemp())) 

adr = '1BSv43wsjcX7C6xfzxnDSV8coCxRAVKrgf'

try:
    with open('smarthome.json', 'r') as f:
        distros_dict = json.load(f) 
        for i in range(len(distros_dict)):
            if distros_dict[i]['DataDeviceType'] == 10 or distros_dict[i]['DataDeviceType'] == 7 or distros_dict[i]['DataDeviceType'] == 4:
                mac = distros_dict[i]['name']
                dictKeys[mac] = distros_dict[i]['key']
                dictNames[mac] = re.sub('[^A-Za-z0-9_]+', '_', distros_dict[i]['DeviceName']) # get clean name
                dictType[mac] = distros_dict[i]['DataDeviceType']
                if distros_dict[i]['DataDeviceType'] == 7:
                    listSD.append(mac.upper())
except:
    print('error reading smarthome.json')
    exit(0)
if len(dictKeys) == 0:
    print('error no sensors found')
    exit(0)

print('Willkommen bei den MSH Tools')
print('Diese SW ist eine technische Demonstration und dient ausschliesslich zum Testen.')
print('Verwendung und Benutzung auf eigene Gefahr und Risiko.')
print('Wenn euch meine Arbeit  gefaellt oder diese SW benutzt dann unterstuetzt mich\n unter der folgenden Bitcoin Adresse: {0}.'.format(adr))

# start Subscriber Thread
if usemqtt:
    subscribe_thread = threading.Thread(target=checkSubscribeMSGs)
    subscribe_thread.start()
# Start BLE processes
os.system("hciconfig hci0 down")
time.sleep(0.5)
os.system("hciconfig hci0 up")
time.sleep(0.5)

process_hcitool = subprocess.Popen(['hcitool', 'lescan', '--passive','--duplicate'], 
                           stdout=subprocess.PIPE,
                           universal_newlines=True)
process_btmon = subprocess.Popen(['btmon'], 
                           stdout=subprocess.PIPE,
                           universal_newlines=True)

process_started = True
process_cnt = 0
while not stopProcess:
    process_cnt = process_cnt + 1
    if int(time.time()*1000) > (cputs + 2500) and (process_cnt % 2) == 0:
        cputs = int(time.time()*1000)
        if cputemp() > 82.0:
            print('High Temperature')
            exit(0)
        processSDrequests()
        if not process_started:
            process_started = True
            os.system("hciconfig hci0 down")
            time.sleep(0.2)
            os.system("hciconfig hci0 up")
            time.sleep(0.2)

            process_hcitool = subprocess.Popen(['hcitool', 'lescan', '--passive','--duplicate'], 
                                       stdout=subprocess.PIPE,
                                       universal_newlines=True)
            process_btmon = subprocess.Popen(['btmon'], 
                                       stdout=subprocess.PIPE,
                                       universal_newlines=True)
            time.sleep(0.2)

    output = process_btmon.stdout.readline()
    output = output.strip()
    if '> HCI Event: LE Meta Event (0x3e' in output:
        startfound = 1
        endfound = 0
        datafound = 0
        debugtext = ''
        timestamp = 0.0
        timestampRT = int(time.time()*1000)
        rssivalue = 0.0
        try:
            timestamp = float(output.split()[-1])
        except:
            print(output)
    if startfound == 1:
        debugtext = debugtext + output +'\n'
    if startfound == 1 and 'Address:' in output:
        mac = output.split()[1].replace(':','')
    if startfound == 1 and 'Data:' in output and 'a0064' in output:
        datafound = 1
        data = output.split()[1]
        data = data.upper()
        data = data[10:]
        data = data[:32]
    if 'RSSI:' in output:
        if 'invalid' in output:
            startfound = 0
            endfound = 0
            datafound = 0
            debugtext = ''
        else:
            endfound = 1
            if startfound == 1:
                try:
                    rssivalue = float(output.split()[1])
                except:
                    startfound = 0
                    endfound = 0
                    datafound = 0
    if endfound == 1:
        if startfound == 1 and datafound == 1 and mac in dictNames and len(data) == 32 and rssivalue < -10:
            # process frame
            key = dictKeys[mac]
            decrMessage = DecryptBLE(data, key, mac)
            if decrMessage.startswith('10') == False or decrMessage.endswith('00000000') == False:
                if debuginfo:
                    print(debugtext)
            else:
                value_int = 0
                intermessage = 'unknown'
                printinfo = False
                initmsg = False
                if mac in dictLast:
                    if dictType[mac] == 10: # 'motion detector':
                        if dictLast[mac] != decrMessage:
                            printinfo = True
                            value_int = int(decrMessage[4:6]+decrMessage[2:4], 16)
                            if decrMessage.endswith('10000000000'):
                                intermessage = 'move'
                            else:
                                intermessage = decrMessage
                    if dictType[mac] == 7: # 'smoke detector':
                        if dictLast[mac] != decrMessage:
                            value_int = 1
                            if decrMessage.endswith('1000000000000000000'):
                                value_int = 1
                                intermessage = 'normal'
                            elif decrMessage.endswith('6000000000000000000'):  
                                intermessage = 'alarm_6'
                            elif decrMessage.endswith('4000000000000000000'):  
                                intermessage = 'alarm_4'
                            else:
                                intermessage = decrMessage
                            printinfo = True
                    if dictType[mac] == 4: # 'door contact':
                        if dictLast[mac][8:] != decrMessage[8:]:
                            value_int = 1
                            printinfo = True
                            if decrMessage.endswith('1000000000000000000'):
                                value_int = 0
                                intermessage = 'close'
                            elif decrMessage.endswith('2000000000000000000'):  
                                intermessage = 'open'
                            else:
                                intermessage = decrMessage
                    dictLast[mac] =  decrMessage       
                else:
                    initmsg = True
                    dictLast[mac]=decrMessage
                    if dictType[mac] == 10: # 'motion detector':
                        printinfo = True
                        value_int = int(decrMessage[4:6]+decrMessage[2:4], 16)
                        if decrMessage.endswith('10000000000'):
                            intermessage = 'init'
                        else:
                            intermessage = decrMessage
                    if dictType[mac] == 7: # 'smoke detector':
                        value_int = 1
                        if decrMessage.endswith('1000000000000000000'):
                            intermessage = 'normal'
                            value_int = 0
                        elif decrMessage.endswith('6000000000000000000'):  
                            intermessage = 'alarm_6'
                        elif decrMessage.endswith('4000000000000000000'):  
                            intermessage = 'alarm_4'
                        else:
                            intermessage = decrMessage
                        printinfo = True
                    if dictType[mac] == 4: # 'door contact':
                        value_int = 1
                        printinfo = True
                        if decrMessage.endswith('1000000000000000000'):
                            intermessage = 'close'
                            value_int = 0
                        elif decrMessage.endswith('2000000000000000000'):  
                            intermessage = 'open'
                        else:
                            intermessage = decrMessage
                    dictLast[mac] = decrMessage

                dictMessage = {}
                dictMessage['mac'] = mac
                dictMessage['source'] = raspiname
                dictMessage['details'] = intermessage
                dictMessage['value'] = value_int
                dictMessage['rssivalue'] = rssivalue
                dictMessage['timestampRT'] = timestampRT
                dictMessage['type'] = dictType[mac]
                dictMessage['name'] = dictNames[mac]
                if printinfo:
                    if debuginfo:
                        print('{0} : {1} : {2} : {3} : {4} : {5} : {6}'.format(mac, intermessage, rssivalue, timestampRT, dictNames[mac],  value_int, cputemp()))
                    if usemqtt:
                        try:
                            publish.single("msh/d_{0}".format(dictNames[mac].lower()), "{0}".format(json.dumps(dictMessage)), hostname="{0}".format(hostadr), auth=auth)
                        except:
                            pass
        startfound = 0
        endfound = 0
        datafound = 0
        debugtext = ''
    # Do something else
if usemqtt:
    subscribe_thread.join()

